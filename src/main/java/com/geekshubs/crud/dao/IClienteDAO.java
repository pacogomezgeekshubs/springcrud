package com.geekshubs.crud.dao;

import com.geekshubs.crud.models.Cliente;

import java.util.List;

public interface IClienteDAO {

    public List<Cliente> findAll();
    public void save(Cliente cliente);
    public Cliente findBy(Long Id);
    public void delete(Long Id);
}