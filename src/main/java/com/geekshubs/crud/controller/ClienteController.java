package com.geekshubs.crud.controller;

import com.geekshubs.crud.dao.IClienteDAO;
import com.geekshubs.crud.models.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.lang.ref.SoftReference;

@Controller
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private IClienteDAO clienteDAO;

    @GetMapping("/lista")
    public String listaClientes(Model model){
        model.addAttribute("titulo", "Listado de clientes");
        model.addAttribute("clientes",clienteDAO.findAll());
        return "clientes/lista";
    }

    @GetMapping("/form")
    public String insertarCliente(@RequestParam(required = false) Long id, Model model){
        System.out.println("-------------- RUTA FORM GET ----------");
        Cliente cliente=null;
        if(id==null){
            cliente=new Cliente();
        }else{
            cliente=clienteDAO.findBy(id);
        }
        model.addAttribute("titulo", "Nuevo Cliente");
        model.addAttribute("cliente",cliente);
        return "clientes/nuevo";
    }
    @GetMapping("/delete")
    public String borrarCliente(@RequestParam(required = false) Long id) {
        if(id!=null)clienteDAO.delete(id);
        return "redirect:/cliente/lista";
    }

    @PostMapping("/form")
    public String salvarCliente(@Valid Cliente cliente, BindingResult result, Model model){
        if(result.hasErrors()){
            System.out.println(cliente);
            model.addAttribute("titulo", "Nuevo Cliente");
            model.addAttribute("cliente",cliente);
            return "clientes/nuevo";
        }
        clienteDAO.save(cliente);
        return "redirect:/cliente/lista";
    }
}
