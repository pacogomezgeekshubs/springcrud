package com.geekshubs.crud.dao;

import com.geekshubs.crud.models.Cliente;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class ClienteRepository implements IClienteDAO{
    @PersistenceContext
    private EntityManager em;

    @Override
    @Transactional(readOnly = true)
    public List<Cliente> findAll() {
        return em.createQuery("SELECT c FROM Cliente c").getResultList();
    }

    @Override
    @Transactional
    public void save(Cliente cliente) {
        System.out.println("-------------- SAVE");
        System.out.println(cliente.getId());
        if (cliente.getId() > 0) {
            em.merge(cliente);
        } else {
            em.persist(cliente);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Cliente findBy(Long Id) {
        return em.find(Cliente.class, Id);
    }

    @Override
    @Transactional()
    public void delete(Long Id) {
        em.remove(findBy(Id));
    }
}
